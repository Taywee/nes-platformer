# Contributing

* All changes will be licensed GPLv3+.
* Stick to the conventions as seen as best as you can (styling and such).
* Don't overcomplicate the build process if it can be avoided.
* Use simple character sheets and rom bank configurations.
    * Don't use a complex character sheet compiler, just hand-craft them for now.